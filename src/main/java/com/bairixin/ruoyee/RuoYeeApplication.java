package com.bairixin.ruoyee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RuoYeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(RuoYeeApplication.class, args);
	}

}
